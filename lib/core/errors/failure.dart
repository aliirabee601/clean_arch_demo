import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  Failure([List proprities = const <dynamic>[]]) : super(proprities);
}

class ServerFailure extends Failure {}

class CacheFailure extends Failure {}

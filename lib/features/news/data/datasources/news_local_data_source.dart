import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/errors/exeptions.dart';
import '../models/news_model.dart';

abstract class NewsLocalDataSource {
  Future<NewsModel> getLastNews();
  Future<void> cacheNews(NewsModel newsModel);
}

const CASHED_NEWS = 'CASHED_NEWS';

class NewsLocalDataSourceImpl implements NewsLocalDataSource {
  final SharedPreferences sharedPreferences;
  NewsLocalDataSourceImpl({this.sharedPreferences});
  @override
  Future<void> cacheNews(NewsModel newsModel) {
    return sharedPreferences.setString(
        CASHED_NEWS, json.encode(newsModel.toJson()));
  }

  @override
  Future<NewsModel> getLastNews() {
    final jsonResponse = sharedPreferences.getString(CASHED_NEWS);
    if (jsonResponse != null) {
      return Future.value(NewsModel.fromJson(json.decode(jsonResponse)));
    } else {
      throw CacheException();
    }
  }
}

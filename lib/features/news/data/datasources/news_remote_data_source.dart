import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../../core/errors/exeptions.dart';
import '../models/news_model.dart';

abstract class NewsRemoteDataSource {
  Future<NewsModel> getNews();
}

class NewsRemoteDataSourceImpl implements NewsRemoteDataSource {
  final http.Client client;
  NewsRemoteDataSourceImpl({this.client});
  @override
  Future<NewsModel> getNews() async {
    final response = await client.get('');
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return NewsModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}

import 'package:flutter/material.dart';
import '../../domain/entities/news.dart';

class NewsModel extends News {
  NewsModel(
      {@required int newsId,
      @required String newsTitle,
      @required int viewsNumber,
      @required String newsImage,
      @required String newsDate})
      : super(
            newsId: newsId,
            newsTitle: newsTitle,
            viewsNumber: viewsNumber,
            newsImage: newsImage,
            newsDate: newsDate);

  factory NewsModel.fromJson(Map<String, dynamic> json) {
    return NewsModel(
        newsId: json['id'],
        viewsNumber: json['views'],
        newsImage: json['image'],
        newsTitle: json['title'],
        newsDate: json['create_at']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': newsId,
      'views': viewsNumber,
      'image': newsImage,
      'title': newsTitle,
      'create_at': newsDate
    };
  }
}

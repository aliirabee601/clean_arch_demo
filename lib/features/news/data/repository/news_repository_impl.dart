import 'package:dartz/dartz.dart';
import 'package:news_app/core/errors/exeptions.dart';

import '../../../../core/errors/failure.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/news.dart';
import '../../domain/repository/news_repository.dart';
import '../datasources/news_local_data_source.dart';
import '../datasources/news_remote_data_source.dart';

class NewsRepositoryImpl implements NewsRepository {
  final NewsRemoteDataSource remoteDataSource;
  final NewsLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  NewsRepositoryImpl(
      {this.localDataSource, this.remoteDataSource, this.networkInfo});
  @override
  Future<Either<Failure, News>> getNews() async {
    if (await networkInfo.isConnecte) {
      try {
        final newsData = await remoteDataSource.getNews();
        localDataSource.cacheNews(newsData);
        return Right(newsData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localNews = await localDataSource.getLastNews();
        return Right(localNews);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}

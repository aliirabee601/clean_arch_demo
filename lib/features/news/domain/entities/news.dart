import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class News extends Equatable {
  final int newsId;
  final String newsTitle;
  final int viewsNumber;
  final String newsImage;
  final String newsDate;
  News(
      {@required this.newsId,
      @required this.newsTitle,
      @required this.viewsNumber,
      @required this.newsImage,
      @required this.newsDate})
      : super([newsId, newsTitle, viewsNumber, newsImage, newsDate]);
}

import 'package:dartz/dartz.dart';

import '../../../../core/errors/failure.dart';
import '../entities/news.dart';

abstract class NewsRepository {
  Future<Either<Failure, News>> getNews();
}
